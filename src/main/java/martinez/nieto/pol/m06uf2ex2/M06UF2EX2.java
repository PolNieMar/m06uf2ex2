/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package martinez.nieto.pol.m06uf2ex2;

import com.github.javafaker.Faker;
import entitats.Customer;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Pol Nieto Martínez
 */
public class M06UF2EX2 {

    private static SessionFactory factory;
    private static Session session;
    private static final Logger logger = LogManager.getLogger(M06UF2EX2.class);

    public static void main(String[] args) {
        try {
            factory = new Configuration().configure("hibernateConfig/hibernate.cfg.xml").buildSessionFactory();
            session = factory.openSession();

            Transaction tx = session.beginTransaction();

            Faker faker = new Faker(new Locale("es"));

            for (int i = 0; i < 1000; i++) {
                Customer customer = new Customer();
                customer.setDni(faker.idNumber().valid());
                customer.setCustomerName(faker.name().fullName());
                customer.setContactLastName(faker.name().lastName());
                customer.setContactFirstName(faker.name().firstName());
                customer.setPhone(faker.phoneNumber().cellPhone());
                customer.setAddressLine1(faker.address().streetAddress());
                customer.setAddressLine2(faker.address().secondaryAddress());
                customer.setCity(faker.address().city());
                customer.setState(faker.address().state());
                customer.setPostalCode(faker.address().zipCode());
                customer.setCountry(faker.address().country());
                customer.setSalesRepEmployeeNumber(faker.number().numberBetween(1000, 9999));
                customer.setCreditLimit((float) faker.number().randomDouble(2, 1, 100000));
                session.save(customer);
            }

            tx.commit();

        } catch (ConstraintViolationException ex) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            System.err.println("Constraint violation: " + ex.getMessage());

        } catch (Exception ex) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            System.err.println("Exception: " + ex.getMessage());
        } finally {
            session.close();

            factory.close();

        }
    }
}